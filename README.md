# emojireport

## Project setup
```
npm install
```

### Compiles and hot-reloads frontend for development
```
npm run serve
```

### Compiles and minifies frontend for production
```
npm run build
```

## Slack Scraper

### .env
Create a .env file in the project root with the following inside it

    SLACK_TOKEN=<your token here>

Visit https://api.slack.com/custom-integrations/legacy-tokens to create a token.

## How to use

### 1. Get custom emoji

This json file has been committed to the repo but it doesn't hurt to update it in case someone's added new emoji.

```js
node cli/index.js emoji
```

### 2. Get channel data
Get data from a channel for a date range like so:

```js
node cli/index.js scrape --channel='hottub' --from=2019-01-05 --to=2019-01-10
```
 
This will write a json file at `./json/hottub-2019-01-05_2019-01-10.json`

### 3. Parse data to generate a stats file

```js
node cli/index.js parse ./json/hottub-2019-01-05_2019-01-10.json
```
  
This will write a json file at `./public/hottub-2019-01-05_2019-01-10-stats.json`

## Lorem Fidsum

To get all messages with the emoji :loremfidsum: do steps 1 and 2 above, then run

```js
node cli/index.js loremfidsum ./json/hottub-2019-01-05_2019-01-10.json
```

THis will write a json file at `./json/hottub-2019-01-05_2019-01-10-loremfidsum.json`

TODO: Refactor this to allow you to specify the emoji to filter by.

## Commands
### Scrape
```
node cli/index.js scrape

  --channel  ID of channel to scrape           [string] [required]
  --from     Start date                        [string] [required]
  --to       End date                          [string] [required]
  --debug    Toggle debug mode                          [boolean]
```

### Parse
```
node cli/index.js parse <pathtofile>
```

- path can be relative or absolute

### Environment Variables Required:

- SLACK_TOKEN
  
Either set inline when running the command, or create a .env file in the project root. See npm `dotenv` package for details

## Stats Measured
- rank emoji by usage
- rank users by emoji usage count
  - also show count of emoji used
  - top five emoji

## Rendering Emoji
- base emoji https://github.com/iamcal/emoji-data
- custom team emoji https://api.slack.com/methods/emoji.list

# To Do
- combine skin-tone variants?
- most frequent user of each emoji
- emojidex
- improve channel view
  - default to showing combined data from all quarters
  - dropdown for year
  - dropdown for quarter
  - display filtered results below rather than in separate view
  - tab for showing totals (as in the existing Stats page but combining multiple quarters)
  - tab for showing summary of each quarter
    - top 5 users, top 5 emoji etc for each quarter
