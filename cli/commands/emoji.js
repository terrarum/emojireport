const scraper = require('../scraper');

exports.command = 'emoji';
exports.describe = 'get custom emoji from the API';

exports.builder = {
  debug: {
    describe: 'Toggle debug mode',
    type: 'boolean',
  },
};

exports.handler = scraper.emoji;
