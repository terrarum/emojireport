const parser = require('../parser');

exports.command = 'loremfidsum <file>';
exports.describe = 'find all messages with the emoji :loremfidsum: ';

exports.builder = {
  debug: {
    describe: 'Toggle debug mode',
    type: 'boolean',
  },
};

/**
 * Handles input validation then calls parse command.
 * @param argv
 */
exports.handler = parser.validate;
