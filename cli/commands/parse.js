const parser = require('../parser');

exports.command = 'parse <file>';
exports.describe = 'process output from the scrape command';

exports.builder = {
  debug: {
    describe: 'Toggle debug mode',
    type: 'boolean',
  },
};

/**
 * Handles input validation then calls parse command.
 * @param argv
 */
exports.handler = parser.validate;
