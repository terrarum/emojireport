const scraper = require('../scraper');

exports.command = 'scrape';
exports.describe = 'pull data from the API';

exports.builder = {
  channel: {
    demandOption: true,
    describe: 'ID of channel to scrape',
    type: 'string'
  },
  from: {
    demandOption: true,
    describe: 'Start date',
    type: 'string'
  },
  to: {
    demandOption: true,
    describe: 'End date',
    type: 'string'
  },
  debug: {
    describe: 'Toggle debug mode',
    type: 'boolean',
  },
};

exports.handler = scraper.validate;
