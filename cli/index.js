const fs = require('fs');

function init() {
  // Create json dir.
  if (!fs.existsSync('./json')) {
    fs.mkdirSync('./json');
  }

  // Load .env file.
  require('dotenv').config();

  // Handle commands.
  require('yargs')
    .commandDir('./commands')
    .help()
    .argv;
}

init();
