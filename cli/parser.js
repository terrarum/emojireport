const fs = require('fs');
const path = require('path');

const messageProcessor = require('./processors/messageProcessor');

function loadJson(filePath) {
  const fileContent = fs.readFileSync(filePath, 'utf8');
  let fileJson = null;

  try {
    fileJson = JSON.parse(fileContent);
  }
  catch (e) {
    console.error(`Invalid JSON in ${filePath}`);
    console.error('');
    console.error(e);
    process.exit();
  }

  return fileJson;
}

function buildProfile(users, userId) {
  const userData = users[userId].user.profile;

  return {
    displayName: userData.display_name,
    avatar: userData.image_192,
    realName: userData.real_name,
  };
}

function parse(filePath, command) {
  console.log('');
  console.log(`Reading file ${filePath}`);
  console.log('');

  const data = loadJson(filePath);

  console.log(`Parsing ${data.messages.length} messages from ${data.meta.channelName} from ${data.meta.from.split('T')[0]} to ${data.meta.to.split('T')[0]}.`);

  // Regular emojireport parssing.
  if (command === 'parse') {
    // Process messages.
    const stats = messageProcessor.processMessages(data.messages);
    stats.meta = data.meta;

    // Attach actual user data.
    stats.users.forEach((user) => {
      user.profile = buildProfile(data.users, user.id);
    });

    let outputFilePath = filePath.replace('.json', '-stats.json');
    outputFilePath = outputFilePath.replace('/json/', '/public/');

    console.log(`\nWriting stats to ${outputFilePath}`);
    fs.writeFileSync(`${outputFilePath}`, JSON.stringify(stats));
  }

  // Pull out :loremfidsum:'d messages.
  if (command === 'loremfidsum') {
    const messages = [];

    data.messages.forEach((message) => {
      if (!message.reactions) {
        return;
      }

      const h = message.reactions.filter(reaction => reaction.name === 'loremfidsum');
      if (h.length && data.users[message.user].user.name === 'james.fidler') {
        messages.push(message.text);
      }
    });

    let outputFilePath = filePath.replace('.json', '-loremfidsum.json');

    console.log(`\nWriting ${messages.length} loremfidsum'd messages to ${outputFilePath}`);
    fs.writeFileSync(`${outputFilePath}`, JSON.stringify(messages));
  }
}

/**
 * Ensure path leads to an actual file before proceeding.
 *
 * @param argv
 */
function validate(argv) {
  const filePath = path.resolve(process.cwd(), argv.file);
  const command = argv._[0];

  if (!fs.existsSync(filePath)) {
    console.log(`File ${filePath} does not exist.`);
    process.exit();
  }

  parse(filePath, command);
}

module.exports = {
  validate,
};
