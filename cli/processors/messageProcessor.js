const standardEmojis = require('emoji-datasource/emoji');
const customEmoji = require('../../src/assets/emoji');

const tracker = require('../tracker');

function getStandardEmoji(emojiName) {
  // Get skin tone ID.
  let skinToneId = null;
  if (emojiName.indexOf('::') !== -1) {
    const emojiNameArray = emojiName.split('::');
    emojiName = emojiNameArray[0];
    const skinTone = emojiNameArray[1];
    const skinToneEmoji = standardEmojis.find(standardEmoji => standardEmoji.short_names.includes(skinTone));
    skinToneId = skinToneEmoji.unified;
  }

  // Get emoji.
  let emoji = standardEmojis.find(standardEmoji => standardEmoji.short_names.includes(emojiName));

  // Get skin tone variant if exists.
  if (skinToneId) {
    emoji = emoji.skin_variations[skinToneId];
  }

  return emoji;
}

function isKnownEmoji(emojiName) {
    // Is custom?
  if (customEmoji[emojiName]) {
    return true;
  }

  // Is standard?
  if (getStandardEmoji(emojiName)) {
    return true;
  }

  return false;
}

/**
 *
 * @param text
 * @returns {Array}
 */
function extractEmojiFromText(text) {
  const regex = /:([^\r\n\t\f\v /"\\<>]+?):/gm;

  const textEmoji = [];

  let m;
  while ((m = regex.exec(text)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
      regex.lastIndex++;
    }

    // The result can be accessed through the `m`-variable.
    m.forEach((match, groupIndex) => {
      if (groupIndex === 1) {
        if (isKnownEmoji(match)) {
          textEmoji.push(match);
        }
      }
    });
  }

  return textEmoji;
}

/**
 *
 * @param {string} message
 */
function process(message) {
  const messageUserId = message.user;

  // Add emoji from text.
  const textEmojis = extractEmojiFromText(message.text);
  textEmojis.forEach((textEmoji) => {
    tracker.addData(messageUserId, textEmoji);
  });

  // Add emoji from reactions.
  if (message.reactions) {
    message.reactions.forEach((reaction) => {
      reaction.users.forEach((userId) => {
        tracker.addData(userId, reaction.name);
      });
    });
  }
}

function processMessages(messages) {
  messages.forEach(message => process(message));

  return tracker.getData();
}

module.exports = {
  process,
  processMessages,
};
