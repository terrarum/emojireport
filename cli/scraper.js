const fs = require('fs');
const slugify = require('slugify');
const dayjs = require('dayjs');

const slack = require('./slack');

const DATE_FORMAT = 'YYYY-MM-DD';

async function getUsers(messages) {
  const users = {};

  // Process messages.
  for (let messageIndex = 0; messageIndex < messages.length; messageIndex += 1) {
    const message = messages[messageIndex];

    // Bail if message has no user (e.g. bot message)
    if (!message.user) {
      continue;
    }

    // Get message user if new.
    if (!users[message.user]) {
      users[message.user] = await slack.getUser(message.user);
    }

    // Get reaction users if new.
    if (message.reactions) {
      for (let reactionIndex = 0; reactionIndex < message.reactions.length; reactionIndex += 1) {
        const reaction = message.reactions[reactionIndex];
        for (let userIndex = 0; userIndex < reaction.users.length; userIndex += 1) {
          const userId = reaction.users[userIndex];
          if (!users[userId]) {
            users[userId] = await slack.getUser(userId);
          }
        }
      }
    }
  }

  return users;
}

/**
 *
 * @param {string} channelName
 * @param {dayjs.Dayjs} from
 * @param {dayjs.Dayjs} to
 */
async function scrape(channelName, from, to) {
  console.log(`\nScrape ${channelName} from ${from.format(DATE_FORMAT)} to ${to.format(DATE_FORMAT)}\n`);

  // Ensure 'to' value is inclusive of given day.
  to = to.endOf('day');

  let channelId = null;
  let channelNameSlug = null;

  // Get channel ID from API.
  console.log('Getting channels');
  const channels = await slack.getChannels(false);
  for (let i = 0; i < channels.length; i++) {
    const channel = channels[i];
    if (channel.name === slugify(channelName, {lower: true})) {

      console.log(`Channel ${channel.name} found\n`);

      channelId = channel.id;
      channelNameSlug = channel.name;
    }
  }

  // Bail if channel not found.
  if (channelId === null) {
    console.log(`Cannot find channel '${slugify(channelName, {lower: true})}'`);
    return;
  }

  // Get all messages.
  const messages = await slack.getChannelMessages(channelId, from, to);
  if (messages.length === 0) {
    console.log('\nNo messages in given date range.');
    return;
  }

  // Get all users.
  console.log('\nGetting users');
  const users = await getUsers(messages);

  const filename = `${channelNameSlug}-${from.format(DATE_FORMAT)}_${to.format(DATE_FORMAT)}.json`;

  console.log('');
  console.log(`Writing ${messages.length} messages and ${Object.keys(users).length} users to ${filename}`);
  const output = {
    meta: {
      channelName: channelNameSlug,
      channelId,
      from,
      to,
    },
    users,
    messages,
  };

  fs.writeFileSync(`./json/${filename}`, JSON.stringify(output));
}

/**
 * Get custom emoji.
 *
 * @returns {Promise<void>}
 */
async function emoji() {
  console.log('\nGetting custom emoji\n');
  const customEmoji = await slack.getCustomEmoji();

  console.log(`\nWriting ${Object.keys(customEmoji).length} emoji to emoji.json`);
  fs.writeFileSync(`./src/assets/emoji.json`, JSON.stringify(customEmoji));
}

/**
 * Validate input for scraper.
 * Call scraper if input is valid.
 *
 * @param argv
 * @returns {Promise<void>}
 */
async function validate(argv) {
  // Set debug globally rather than passing it around.
  process.debug = argv.debug || false;

  const channel = argv.channel;

  // Convert to date objects.
  const from = dayjs(argv.from);
  const to = dayjs(argv.to);

  // Bail if from date is invalid.
  if (!from.isValid()) {
    console.error('From date is invalid. Must be parseable by Date.');
    return;
  }

  // Bail if to date is invalid.
  if (!to.isValid()) {
    console.error('To date is invalid. Must be parseable by Date.');
    return;
  }

  await scrape(channel, from, to);
}

module.exports = {
  validate,
  emoji,
};
