const fetch = require('node-fetch');

const debug = require('./helpers/debug');

const SLACK_API_BASE = 'https://slack.com/';
const SLACK_OAUTH_ACCESS_TOKEN = process.env.SLACK_TOKEN;

if (!SLACK_OAUTH_ACCESS_TOKEN) {
  console.log('SLACK_TOKEN env var required. See readme for details.');
  process.exit(1);
}

/**
 * Allows halting the thread to throttle API connections.
 *
 * @param duration
 * @returns {Promise<any>}
 */
function pause(duration) {
  debug(`Pausing for ${duration}ms`);
  return new Promise((resolve) => {
    setTimeout(() => resolve(), duration);
  })
}

/**
 * Get JSON from the given URL, with a delay.
 *
 * @param {URL} url
 * @param {boolean} delay
 * @returns {Promise<{ok}|void|T>}
 */
async function getJson(url, delay = true) {
  url.searchParams.set('limit', '100');
  url.searchParams.set('inclusive', 'true');
  url.searchParams.set('token', SLACK_OAUTH_ACCESS_TOKEN);

  if (delay) {
    await pause(500);
  }
  let response = await fetch(url.href).then(res => res.json()).catch((err => console.log(err)));

  let specific = '';

  if (url.searchParams.get('cursor')) {
    specific = url.searchParams.get('cursor');
  }
  if (url.searchParams.get('ts')) {
    specific = url.searchParams.get('ts');
  }
  if (url.searchParams.get('user')) {
    specific = url.searchParams.get('user');
  }

  console.log(`GET request: ${url.pathname} ${specific}`);
  debug(`response.ok: ${response.ok}\nURL: ${url.href}`);
  if (!response.ok) {
    console.log(response.error);
    console.log('Waiting for one minute.');
    await pause(60000);
    response = await fetch(url.href).then(res => res.json()).catch((err => console.log(err)));
  }
  return response;
}

/**
 * Page through the given URL.
 *
 * @param {URL} url
 * @param {string} cursor
 * @param {array} pages
 * @returns {Promise<void>}
 */
async function pageUrl(url, cursor = '', pages = []) {
  if (cursor) {
    url.searchParams.set('cursor', cursor);
  }

  let page = await getJson(url);
  pages.push(page);
  if (page.response_metadata && page.response_metadata.next_cursor) {
    const cursor = page.response_metadata.next_cursor;
    return pageUrl(url, cursor, pages)
  }
  else {
    const results = [];
    pages.forEach((pageResult) => {
      results.push(...pageResult.messages)
    });

    return results;
  }
}

/**
 * Get all replies to the given message.
 *
 * @param message
 * @param channelId
 * @returns {Promise<void>}
 */
async function getThreadMessages(message, channelId) {
  const url = new URL('/api/conversations.replies', SLACK_API_BASE);
  url.searchParams.append('channel', channelId);
  url.searchParams.append('ts', message.ts);

  return await pageUrl(url);
}

/**
 * Get all messages from the given channel and date range.
 *
 * @param channelId
 * @param {dayjs.Dayjs} from
 * @param {dayjs.Dayjs} to
 * @returns {Promise<void>}
 */
async function getChannelMessages(channelId, from, to) {
  const url = new URL('/api/conversations.history', SLACK_API_BASE);
  url.searchParams.append('channel', channelId);
  url.searchParams.append('oldest', from.unix());
  url.searchParams.append('latest', to.unix());

  // Get channel messages.
  console.log('');
  console.log('Getting messages');
  let messages = await pageUrl(url);

  // Get thread messages.
  const threadedMessages = messages.filter(message => message.replies);
  if (threadedMessages.length > 0) {
    console.log('');
    console.log('Getting replies');
  }
  for(let i = 0; i < threadedMessages.length; i++) {
    const threadedMessage = threadedMessages[i];
    let threadReplies = await getThreadMessages(threadedMessage, channelId);

    // Remove all occurrences of parent message.
    threadReplies = threadReplies.filter(reply => reply.ts !== reply.thread_ts);
    messages.push(...threadReplies);
  }

  // Remove files from message - not needed, huge.
  messages.forEach((message) => {
    delete message.files;
  });

  return messages;
}

async function getUser(userId) {
  const url = new URL('/api/users.info', SLACK_API_BASE);
  url.searchParams.append('user', userId);

  return await getJson(url);
}

/**
 * Returns a list of channels.
 *
 * @param {boolean} delay
 * @returns {Promise<{ok}|void|T>}
 */
async function getChannels(delay = true) {
  const url = new URL('/api/conversations.list', SLACK_API_BASE);
  const response = await getJson(url, delay);
  return response.channels;
}

async function getCustomEmoji(delay = false) {
  const url = new URL('/api/emoji.list', SLACK_API_BASE);
  const response = await getJson(url, delay);
  return response.emoji;
}

module.exports = {
  getChannels,
  getChannelMessages,
  getUser,
  getCustomEmoji,
};
