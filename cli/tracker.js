const data = {
  emoji: {},
  users: {},
};

/**
 * Update emoji usage list.
 *
 * @param userId
 * @param emoji
 */
function updateEmojiUsageCount(userId, emoji) {
  // Create emoji if not exist.
  if (!data.emoji[emoji]) {
    data.emoji[emoji] = {
      name: emoji,
      count: 0,
    };
  }

  // Increment emoji count.
  data.emoji[emoji].count += 1;
}

function updateUserEmojiUsage(userId, emoji) {
  // Create user if not exist.
  if (!data.users[userId]) {
    data.users[userId] = {
      id: userId,
      profile: null,
      stats: {
        emoji: {},
        emojiUsed: 0,
        emojiUsages: 0,
      },
    }
  }

  const user = data.users[userId];

  // Create emoji if not exist.
  if (!user.stats.emoji[emoji]) {
    user.stats.emoji[emoji] = {
      name: emoji,
      count: 0
    };
  }

  // Increment emoji count.
  user.stats.emoji[emoji].count += 1;
  user.stats.emojiUsages += 1;
  user.stats.emojiUsed = Object.keys(user.stats.emoji).length;
}

function addData(userId, emoji) {
  updateEmojiUsageCount(userId, emoji);
  updateUserEmojiUsage(userId, emoji);
}

function getData() {
  const users = Object.values(data.users).sort((a, b) => b.stats.emojiUsages - a.stats.emojiUsages);
  users.forEach((user) => {
    user.stats.emoji = Object.values(user.stats.emoji).sort((a, b) => b.count - a.count);
  });

  return {
    emoji: Object.values(data.emoji).sort((a, b) => b.count - a.count),
    users,
  };
}

module.exports = {
  addData: addData,
  getData,
};
