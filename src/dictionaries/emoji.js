import customEmojis from '@/assets/emoji.json';
import standardEmojis from 'emoji-datasource/emoji.json';

/**
 * Gets a custom emoji. Tries again if emoji is an alias.
 *
 * @param emojiName
 * @returns {{custom: boolean, url: *}|boolean}
 */
function getCustomEmoji(emojiName) {
   // Return custom emoji.
  if (customEmojis[emojiName]) {
    const url = customEmojis[emojiName];

    // If emoji is an alias, go around again.
    if (url.startsWith('alias:')) {
      return getCustomEmoji(url.split('alias:')[1]);
    }

    return {
      custom: true,
      url,
    }
  }

  return false;
}

/**
 *
 * @param emojiName
 * @returns {{custom: boolean, x: number, y: number}}
 */
function getStandardEmoji(emojiName) {
  // Get skin tone ID.
  let skinToneId = null;
  if (emojiName.indexOf('::') !== -1) {
    const emojiNameArray = emojiName.split('::');
    emojiName = emojiNameArray[0];
    const skinTone = emojiNameArray[1];
    const skinToneEmoji = standardEmojis.find(standardEmoji => standardEmoji.short_names.includes(skinTone));
    skinToneId = skinToneEmoji.unified;
  }

  // Get emoji.
  let emoji = standardEmojis.find(standardEmoji => standardEmoji.short_names.includes(emojiName));

  // Get skin tone variant if exists.
  if (skinToneId) {
    emoji = emoji.skin_variations[skinToneId];
  }

  return emoji;
}

/**
 *
 * @param emoji
 * @returns {{custom: boolean, x: number, y: number}}
 */
function getStandardEmojiCoords(emoji) {
  // Calculate spritesheet coordinates.
  const spriteSize = 32;
  const spriteGutter = 1;
  const x = spriteGutter + emoji.sheet_x * spriteSize + emoji.sheet_x * (spriteGutter * 2);
  const y = spriteGutter + emoji.sheet_y * spriteSize + emoji.sheet_y * (spriteGutter * 2);

  return {
    custom: false,
    x: -x,
    y: -y,
  }
}

export default function(emojiName) {
  // Get custom emoji.
  let emoji = getCustomEmoji(emojiName);
  if (emoji) {
    return emoji;
  }

  // Get standard emoji.
  emoji = getStandardEmoji(emojiName);
  return getStandardEmojiCoords(emoji);
}
