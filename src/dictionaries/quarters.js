export default {
  'hottub-2019-0': 'hottub-2019-01-01_2019-12-31-stats.json',
  'hottub-2019-1': 'hottub-2019-01-01_2019-03-31-stats.json',
  'hottub-2019-2': 'hottub-2019-04-01_2019-06-30-stats.json',
  'hottub-2019-3': 'hottub-2019-07-01_2019-09-30-stats.json',
  'hottub-2019-4': 'hottub-2019-10-01_2019-12-31-stats.json',
  'fabric-general-2019-0': 'fabric-general-2019-01-01_2019-12-31-stats.json',
  'fabric-general-2019-1': 'fabric-general-2019-01-01_2019-03-31-stats.json',
  'fabric-general-2019-2': 'fabric-general-2019-04-01_2019-06-30-stats.json',
  'fabric-general-2019-3': 'fabric-general-2019-07-01_2019-09-30-stats.json',
  'fabric-general-2019-4': 'fabric-general-2019-10-01_2019-12-31-stats.json',
}
