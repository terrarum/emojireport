import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { MdButton, MdToolbar, MdContent, MdList, MdTooltip, MdCard } from 'vue-material/dist/components'

import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(MdButton);
Vue.use(MdToolbar);
Vue.use(MdContent);
Vue.use(MdList);
Vue.use(MdTooltip);
Vue.use(MdCard);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
