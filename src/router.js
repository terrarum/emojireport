import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/channel/:channel',
      name: 'channel',
      props: true,
      component: () => import(/* webpackChunkName: "quarter" */ './views/Quarter.vue'),
    },
    {
      path: '/channel/:channel/year/:year/quarter/:quarter',
      name: 'stats',
      props: true,
      component: () => import(/* webpackChunkName: "quarter" */ './views/Stats.vue'),
    }
  ]
})
