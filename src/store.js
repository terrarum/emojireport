import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    channel: null,
    channelMeta: null,
    data: {},
    stats: {
      emoji: [],
      users: [],
    },
  },
  mutations: {
    setEmoji(state, emoji) {
      state.stats.emoji = emoji;
    },
    setUsers(state, users) {
      state.stats.users = users;
    },
    setChannel(state, channel) {
      state.channel = channel;
    },
    setChannelMeta(state, channelMeta) {
      state.channelMeta = channelMeta;
    },
    setData(state, { path, data }) {
      state.data[path] = data;
    }
  },
  actions: {
    setEmoji({commit}, emoji) {
      commit('setEmoji', emoji);
    },
    setUsers({commit}, users) {
      commit('setUsers', users);
    },
    setChannel({commit}, channel) {
      commit('setChannel', channel);
    },
    setChannelMeta({commit}, channelMeta) {
      commit('setChannelMeta', channelMeta);
    },
    async loadData({dispatch, commit, getters}, path) {
      let data = getters.getData(path);

      if (data === false) {
        data = await axios.get(path).then(result => result.data);
        commit('setData', { path, data });
      }

      dispatch('setEmoji', data.emoji);
      dispatch('setUsers', data.users);
      dispatch('setChannelMeta', data.meta);
    },
  },
  getters: {
    emoji: (state) => state.stats.emoji,
    users: (state) => state.stats.users,
    meta: (state) => state.channelMeta,
    channel: (state) => state.channel,
    prettyChannel: (state) => {
      return state.channel ? state.channel.replace('-', ' ') : '';
    },
    getData: (state) => (path) => {
      if (state.data[path]) {
        return state.data[path];
      }
      return false;
    }
  },
});
